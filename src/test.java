import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;

import org.junit.Test;

import XML.ChargeurMagasin;
import donnees.Magasin;


public class test {

	@Test
	public void testChargerMagasin() throws FileNotFoundException {
		String repertoire = "src/test";
		ChargeurMagasin cm = new ChargeurMagasin(repertoire);
		try {
		Magasin mag = cm.chargerMagasin();
		} catch(FileNotFoundException e) {
			assertEquals("repertoire inexistant",repertoire+" inexistant",e.getMessage());
		}
	}

}
