package donnees;

import java.util.ArrayList;

/**
 * La classe Magasin represente un magasin qui vend des CDs.
 * </p>
 * 
 * cette classe est caracterisee par un ensemble de CDs correspondant aux CDS
 * vendus dans ce magasin.
 * 
 */
public class Magasin {

	/**
	 * la liste des CDs disponibles en magasin
	 */
	private ArrayList<CD> listeCds;

	/**
	 * construit un magasin par defaut qui ne contient pas de CD
	 */
	public Magasin() {
		listeCds = new ArrayList<CD>();
	}

	/**
	 * ajoute un cd au magasin
	 * 
	 * @param cdAAjouter
	 *            le cd a ajouter
	 */
	public void ajouteCd(CD cdAAjouter) {
		listeCds.add(cdAAjouter);
	}

	@Override
	/**
	 * affiche le contenu du magasin
	 */
	public String toString() {
		String chaineResultat = "";
		// parcours des Cds
		for (int i = 0; i < listeCds.size(); i++) {
			chaineResultat += listeCds.get(i);
		}
		chaineResultat += "nb Cds: " + listeCds.size();
		return (chaineResultat);

	}

	/**
	 * @return le nombre de Cds du magasin
	 */
	public int getNombreCds() {
		return listeCds.size();
	}

	/**
	 * permet d'acceder � un CD
	 * 
	 * @return le cd a l'indice i ou null si indice est non valide
	 */
	public CD getCd(int i) {
		CD res = null;
		if ((i >= 0) && (i < this.listeCds.size()))
			res = this.listeCds.get(i);
		return (res);
	}

	public void trierAlbum() {
		CD min = listeCds.get(0);
		int indicemin;
		ArrayList<CD> listtriee = new ArrayList<CD>();
		for (int j = 0; j < listeCds.size(); j++) {
			min = listeCds.get(0);
			indicemin = 0;
			for (int i = 1; i < listeCds.size(); i++) {
				if (listeCds.get(i).compALB(min) == -1) {
					min = listeCds.get(i);
					indicemin = i;
				}
			}
			listtriee.add(min);
			listeCds.remove(indicemin);
		}
		listeCds = listtriee;
	}

	public void trierAlbum2(ComparateurAlbum ca) {
		CD un = listeCds.get(0);
		CD deux = listeCds.get(1);
		CD aComparer;
		ArrayList<CD> listtriee = new ArrayList<CD>();
		System.out.println(un);
		System.out.println(deux);
		if (ca.etreAvant(un, deux)) {
			listtriee.add(un);
			listtriee.add(deux);
		} else {
			listtriee.add(deux);
			listtriee.add(un);
		}

		System.out.println(listtriee);
		for (int i = 2; i < listeCds.size(); i++) {
			aComparer = listeCds.get(i);
			int size = listtriee.size();
			int j = 0;
			while (size == listtriee.size() && j < size) {
				
				// for(int j = 0 ; j < size; j++) {
				// System.out.println(aComparer);
				//System.out.println(listtriee);
				// System.out.println(ca.etreAvant(aComparer, listtriee.get(j)));
				if (ca.etreAvant(aComparer, listtriee.get(j))) {
					listtriee.add(listtriee.get(size - 1));
					for (int k = size - 1; k > j; k--) {
						listtriee.set(k, listtriee.get(k - 1));
					}
					listtriee.set(j, aComparer);
					// System.out.println(listtriee);
				}
				j++;
			}
		}
		listeCds = listtriee;
	}

	public void trierArtiste() {
		CD min = listeCds.get(0);
		int indicemin;
		ArrayList<CD> listtriee = new ArrayList<CD>();
		for (int j = 0; j < listeCds.size(); j++) {
			min = listeCds.get(0);
			indicemin = 0;
			for (int i = 1; i < listeCds.size(); i++) {
				if (listeCds.get(i).compART(min) == -1) {
					min = listeCds.get(i);
					indicemin = i;
				}
			}
			listtriee.add(min);
			listeCds.remove(indicemin);
		}
		listeCds = listtriee;

	}

	public ArrayList<CD> getListeCds() {
		return listeCds;
	}

	public void setListeCds(ArrayList<CD> listeCds) {
		this.listeCds = listeCds;
	}

}
